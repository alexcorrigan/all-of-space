import os
import pygame

pygame.font.init()
pygame.mixer.init()

WIDTH, HEIGHT = 900, 600
WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))
WALL_WIDTH = 5
WALL = pygame.Rect((WIDTH/2) - (WALL_WIDTH/2), 0, WALL_WIDTH, HEIGHT)

HEALTH_FONT = pygame.font.SysFont("Terminus (TTF)", 30)
VICTORY_FONT = pygame.font.SysFont("Terminus (TTF)", 60)

pygame.display.set_caption('Space!!!')

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
DARK_BLUE = (26, 53, 85)

SPACESHIP_WIDTH = 52
SPACESHIP_HEIGHT = 48
SPACESHIP_DIMENSION = (SPACESHIP_WIDTH, SPACESHIP_HEIGHT)
SPACESHIP_RED_START_POSITION = (100, (HEIGHT / 2) - (SPACESHIP_HEIGHT/2))
SPACESHIP_BLUE_START_POSITION = ((WIDTH - 100) - SPACESHIP_WIDTH, (HEIGHT / 2) - (SPACESHIP_HEIGHT/2))

STAR_FIELD_IMAGE = pygame.transform.scale(pygame.image.load(os.path.join('assets', 'space_background.png')), (WIDTH, HEIGHT))

SPACESHIP_RED_IMAGE = pygame.image.load(os.path.join('assets', 'spaceship_red.png'))
SPACESHIP_BLUE_IMAGE = pygame.image.load(os.path.join('assets', 'spaceship_blue.png'))

BULLET_WIDTH = 18
BULLET_HEIGHT = 6
BULLET_DIMENSION = (BULLET_HEIGHT, BULLET_WIDTH)
BULLET_RED_IMAGE = pygame.image.load(os.path.join('assets', 'laserRed06.png'))
BULLET_BLUE_IMAGE = pygame.image.load(os.path.join('assets', 'laserBlue06.png'))

SPACESHIP_RED = pygame.transform.rotate(pygame.transform.scale(SPACESHIP_RED_IMAGE, SPACESHIP_DIMENSION), -90)
SPACESHIP_BLUE = pygame.transform.rotate(pygame.transform.scale(SPACESHIP_BLUE_IMAGE, SPACESHIP_DIMENSION), 90)

BULLET_RED = pygame.transform.rotate(pygame.transform.scale(BULLET_RED_IMAGE, BULLET_DIMENSION), -90)
BULLET_BLUE = pygame.transform.rotate(pygame.transform.scale(BULLET_BLUE_IMAGE, BULLET_DIMENSION), 90)

BULLET_FIRE_SOUND = pygame.mixer.Sound(os.path.join('assets', 'laserLarge_000.ogg'))
BULLET_HIT_SOUND = pygame.mixer.Sound(os.path.join('assets', 'explosionCrunch_000.ogg'))

MAX_INFLIGHT_BULLETS = 1

FPS = 60
SPACESHIP_VELOCITY = 5
BULLET_VELOCITY = 20

RED_HIT = pygame.USEREVENT + 1
BLUE_HIT = pygame.USEREVENT + 2


def draw_window(red, blue, red_bullets, blue_bullets, red_health, blue_health):
	# WINDOW.fill(DARK_BLUE)
	WINDOW.blit(STAR_FIELD_IMAGE, (0,0))
	pygame.draw.rect(WINDOW, BLACK, WALL)
	WINDOW.blit(SPACESHIP_RED, (red.x, red.y))
	WINDOW.blit(SPACESHIP_BLUE, (blue.x, blue.y))
	for red_bullet in red_bullets:
		WINDOW.blit(BULLET_RED, (red_bullet.x, red_bullet.y))

	for blue_bullet in blue_bullets:
		WINDOW.blit(BULLET_BLUE, (blue_bullet.x, blue_bullet.y))

	red_health_bar = HEALTH_FONT.render("RED HEALTH: " + str(red_health), True, WHITE)
	blue_health_bar = HEALTH_FONT.render("BLUE HEALTH: " + str(blue_health), True, WHITE)

	WINDOW.blit(red_health_bar, (10, 10))
	WINDOW.blit(blue_health_bar, (WIDTH - blue_health_bar.get_width() - 10, 10))

	pygame.display.update()


def red_movement(red, keys_pressed):
	if keys_pressed[pygame.K_a] and red.x - SPACESHIP_VELOCITY > 0:
		red.x -= SPACESHIP_VELOCITY

	if keys_pressed[pygame.K_d] and red.x + red.width + SPACESHIP_VELOCITY < WALL.x:
		red.x += SPACESHIP_VELOCITY

	if keys_pressed[pygame.K_w] and red.y - SPACESHIP_VELOCITY > 0:
		red.y -= SPACESHIP_VELOCITY

	if keys_pressed[pygame.K_s] and red.y + red.height + SPACESHIP_VELOCITY < HEIGHT:
		red.y += SPACESHIP_VELOCITY


def blue_movement(blue, keys_pressed):
	if keys_pressed[pygame.K_LEFT] and blue.x - SPACESHIP_VELOCITY > WALL.x + WALL.width:
		blue.x -= SPACESHIP_VELOCITY

	if keys_pressed[pygame.K_RIGHT] and blue.x + blue.width + SPACESHIP_VELOCITY < WIDTH:
		blue.x += SPACESHIP_VELOCITY

	if keys_pressed[pygame.K_UP] and blue.y - SPACESHIP_VELOCITY > 0:
		blue.y -= SPACESHIP_VELOCITY

	if keys_pressed[pygame.K_DOWN] and blue.y + blue.height + SPACESHIP_VELOCITY < HEIGHT:
		blue.y += SPACESHIP_VELOCITY


def handle_bullets(red_bullets, blue_bullets, red, blue):
	for red_bullet in red_bullets:
		red_bullet.x += BULLET_VELOCITY
		if blue.colliderect(red_bullet):
			print("RED hit!")
			pygame.event.post(pygame.event.Event(BLUE_HIT))
			red_bullets.remove(red_bullet)

		elif (red_bullet.x + BULLET_WIDTH) > WIDTH:
			print("RED miss")
			red_bullets.remove(red_bullet)

	for blue_bullet in blue_bullets:
		blue_bullet.x -= BULLET_VELOCITY
		if red.colliderect(blue_bullet):
			print("BLUE hit!")
			pygame.event.post(pygame.event.Event(RED_HIT))
			blue_bullets.remove(blue_bullet)

		elif blue_bullet.x < 0:
			print("BLUE miss")
			blue_bullets.remove(blue_bullet)


def draw_winner(victory_text):
	victory_display = VICTORY_FONT.render(victory_text, True, WHITE)
	WINDOW.blit(victory_display, (WIDTH/2 - victory_display.get_width()/2, HEIGHT/2 - victory_display.get_height()/2))
	pygame.display.update()
	pygame.time.delay(5000)


def main():

	red_health = 10
	blue_health = 10

	red = pygame.Rect(100, (HEIGHT/2) - (SPACESHIP_HEIGHT/2), SPACESHIP_WIDTH, SPACESHIP_HEIGHT)
	blue = pygame.Rect(WIDTH - 100 - SPACESHIP_WIDTH, (HEIGHT/2) - (SPACESHIP_HEIGHT/2), SPACESHIP_WIDTH, SPACESHIP_HEIGHT)
	red_bullets = []
	blue_bullets = []

	clock = pygame.time.Clock()
	run = True
	
	while run:
		clock.tick(FPS)
		for event in pygame.event.get():

			if event.type == pygame.QUIT:
				run = False
				pygame.quit()

			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_LCTRL and len(red_bullets) < MAX_INFLIGHT_BULLETS:
					print("RED fired")
					BULLET_FIRE_SOUND.play()
					red_bullet = pygame.Rect(red.x + red.width, red.y + (SPACESHIP_HEIGHT//2), BULLET_WIDTH, BULLET_HEIGHT)
					red_bullets.append(red_bullet)

				if event.key == pygame.K_RCTRL and len(blue_bullets) < MAX_INFLIGHT_BULLETS:
					print("BLUE fired")
					BULLET_FIRE_SOUND.play()
					blue_bullet = pygame.Rect(blue.x, blue.y + (SPACESHIP_HEIGHT//2), BULLET_WIDTH, BULLET_HEIGHT)
					blue_bullets.append(blue_bullet)

			if event.type == RED_HIT:
				BULLET_HIT_SOUND.play()
				red_health -= 1

			if event.type == BLUE_HIT:
				BULLET_HIT_SOUND.play()
				blue_health -= 1

		victory_text = ''

		if red_health <= 0:
			victory_text = "BLUE WINS ALL OF SPACE!!!"

		if blue_health <= 0:
			victory_text = "RED WINS ALL OF SPACE!!!"

		if victory_text != '':
			draw_winner(victory_text)
			break

		keys_pressed = pygame.key.get_pressed()

		red_movement(red, keys_pressed)
		blue_movement(blue, keys_pressed)

		handle_bullets(red_bullets, blue_bullets, red, blue)

		draw_window(red, blue, red_bullets, blue_bullets, red_health, blue_health)

	main()


if __name__ == '__main__':
	main()
