```
    _    _     _        ___  _____   ____  ____   _    ____ _____ 
   / \  | |   | |      / _ \|  ___| / ___||  _ \ / \  / ___| ____|
  / _ \ | |   | |     | | | | |_    \___ \| |_) / _ \| |   |  _|  
 / ___ \| |___| |___  | |_| |  _|    ___) |  __/ ___ \ |___| |___ 
/_/   \_\_____|_____|  \___/|_|     |____/|_| /_/   \_\____|_____|
```

![game.png](game.png)

Inspired and guided by "Tech With Tim" in [this](https://www.youtube.com/watch?v=jO6qQDNa2UY) tutorial ([GitHub Repo](https://github.com/techwithtim/PygameForBeginners)).

Game visual and sound assets sourced from:
- Spaceships: https://guardian5.itch.io/blue-green-and-red-spacecraft-asset
- Laser Canons: https://kenney.nl/assets/space-shooter-redux
- https://kenney.nl/assets/space-shooter-extension
- Sound FX: https://kenney.nl/assets/sci-fi-sounds

... and the awesome space background generator from [https://deep-fold.itch.io/space-background-generator](https://deep-fold.itch.io/space-background-generator).


